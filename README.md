# CTOON API Docs
https://api.ctoon.network/

## How to use
**Install aglio**
```bash
yarn global add aglio # or `npm install -g aglio`
```

**Write**
```bash
aglio -i ctoon.apib -s
```

**Build**
```bash
aglio -i ctoon.apib -o index.html
```
